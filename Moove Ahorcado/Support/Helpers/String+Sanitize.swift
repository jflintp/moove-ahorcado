//
//  String+Sanitize.swift
//  Moove Ahorcado
//
//  Created by SFY on 29/04/2020.
//  Copyright © 2020 SFY. All rights reserved.
//

import Foundation

extension String {
    private func removeNonLettersAndNumbers(_ string: String) -> String {
        return string.filter { $0.isLetter || $0.isNumber }.lowercased()
    }
    
    //
    public func sanitize(usingAlphabet alphabet: [Character]) -> String {
        let sanitizedString = removeNonLettersAndNumbers(self)
        var resultString = [Character]()
        for character in sanitizedString {
            if alphabet.contains(character) || character.isNumber {
                resultString.append(character)
            } else {
                let diacriticStrippedCharacter = Character(String(character).folding(options: .diacriticInsensitive, locale: .current))
                if alphabet.contains(diacriticStrippedCharacter) {
                    resultString.append(diacriticStrippedCharacter)
                }
            }
        }
        
        return String(resultString)
    }
}
