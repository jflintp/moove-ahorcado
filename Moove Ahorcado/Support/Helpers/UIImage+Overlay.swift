//
//  UIImage+Overlay.swift
//  Moove Ahorcado
//
//  Created by SFY on 17/04/2020.
//  Copyright © 2020 SFY. All rights reserved.
//

import UIKit

extension UIImage {
    /// Helper method to allow the overlay of an image ontop of another image.
    ///
    /// Can be used repeatidly to create a composition if Images have alpha.
    /// - Parameter overlay: image to be overlayed ontop of the original image.
    /// - Returns: `optional` resulting image from overlaying the `overlay` ontop of `self`
    func overlayed(with overlay: UIImage) -> UIImage? {
        defer {
            UIGraphicsEndImageContext()
        }
        
        UIGraphicsBeginImageContextWithOptions(size, false, scale)
        draw(in: CGRect(origin: CGPoint.zero, size: size))
        overlay.draw(in: CGRect(origin: CGPoint.zero, size: size))
        
        return UIGraphicsGetImageFromCurrentImageContext()
    }
}
