//
//  GameEngine.swift
//  Moove Ahorcado
//
//  Created by SFY on 08/04/2020.
//  Copyright © 2020 SFY. All rights reserved.
//

import Foundation

/// Delagate to the **GameEngine**.
///
/// Will receive status updates relevant to the game.
protocol GameEngineDelegate: AnyObject {
    
    /// Called when the goal word has been changed.
    /// - Parameter word: The raw value of the goal word
    func currentWordChanged(to word: String)
    
    
    /// Called after a turn has been played and determined to be
    /// - Parameters:
    ///   - incorrectGuesses: total number of incorrect gusses up to now.
    ///   - movesLeft: moves left before the game is over.
    func incorrectGuess(incorrectGuesses: Int, movesLeft: Int)
    
    /// Called every time the internet game timer changes.
    /// - Parameter timeLeft: The time remaining on the internal game timer in seconds.
    func countdownTimerChanged(timeLeft: Int)
    
    
    /// Called when the internal timer has run out of time.
    /// - Parameters:
    ///   - timeLeft: The time remaining on the internal game timer in seconds.
    ///   - score: Points given for unsuccessful game.
    func countdownTimerEnded(timeLeft: Int, points: Int)
    
    /// Called when the game has been completed successfully.
    /// - Parameter [points]: Points given for successful game.
    func gameCompleted(points: Int)
    
    
    /// Called when no more turns are available.
    ///
    /// Currently the game allows for 7 incorrect guesses.
    /// - Parameter points: Points given for unsuccessful game.
    func gameOver(points: Int)
}

//MARK: -

class GameEngine {
    
    enum GameState {
        case ready, playing, gameOver, completed
    }
    
    //MARK:  Properties
    
    weak var delegate: GameEngineDelegate?
    
    let maxMoves = 7
    
    fileprivate var gameTimer: Timer?
    fileprivate let targetWord: [Character]
    let pointsForSuccessfulGame: Int
    let pointsForFailedGame: Int
    let alphabet: [Character]
    
    let targetWordContainsNumber: Bool
    
    private var state = GameState.playing {
        didSet {
            switch state {
            case .completed:
                gameTimer?.invalidate()
                delegate?.gameCompleted(points: pointsForSuccessfulGame)
            case .gameOver:
                gameTimer?.invalidate()
                delegate?.gameOver(points: pointsForFailedGame)
            case .playing:
                gameTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updateCountdownTimer), userInfo: nil, repeats: true)
                
            case .ready:
                break
            }
        }
    }
    
    private var timeLeft: Int {
        didSet {
            if timeLeft == 0 {
                gameTimer?.invalidate()
                delegate?.countdownTimerEnded(timeLeft: timeLeft, points: pointsForFailedGame)
            } else {
                delegate?.countdownTimerChanged(timeLeft: timeLeft)
            }
        }
    }
    
    private(set) var currentWord: [Character] {
        didSet {
            delegate?.currentWordChanged(to: String(currentWord))
        }
    }
    
    private var incorrectGuessCount = 0 {
        didSet {
            delegate?.incorrectGuess(incorrectGuesses: incorrectGuessCount, movesLeft: maxMoves - incorrectGuessCount)
            if incorrectGuessCount == maxMoves { state = .gameOver }
        }
    }
    
    //MARK:  Initialization
    
    init(targetWord: [Character] = [Character](), alphabet: [Character] = Alphabets.getUserLocalizedAlphabet(), timeLimit: Int = 0, pointsForCompleted: Int = 0, pointsForFailedGame: Int = 0) {
        let isNumberFlag: Bool = {
            var isNumber = false
            for character in targetWord {
                if character.isNumber {
                    isNumber = character.isNumber
                    return isNumber
                }
            }
            return isNumber
        }()
        
        self.targetWord = targetWord
        self.targetWordContainsNumber = isNumberFlag
        self.alphabet = alphabet
        self.timeLeft = timeLimit
        self.pointsForSuccessfulGame = pointsForCompleted
        self.pointsForFailedGame = pointsForFailedGame
        
        currentWord = Array(repeating: "_", count: targetWord.count)
    }
    
    convenience init(data: [String:String]) {
        let targetWord: String = data["Palabra"]?.lowercased() ?? ""
        let timeLimit = Int(data["timeLimit"] ?? "") ?? 0
        let pointsForCompleted = Int(data["puntosBien"] ?? "") ?? 0
        let pointsForFailedGame = Int(data["puntosMal"] ?? "") ?? 0
        let targetAlphabet: [Character] = Alphabets.getUserLocalizedAlphabet()
        
        self.init(targetWord: Array(targetWord.sanitize(usingAlphabet: targetAlphabet)), alphabet: targetAlphabet, timeLimit: timeLimit, pointsForCompleted: pointsForCompleted, pointsForFailedGame: pointsForFailedGame)
    }
    
    //MARK:  Private
    
    private  func hasWordBeenCompleted() {
        if currentWord == targetWord {
            state = .completed
        }
    }
    
    private  func correctGuess(_ inputCharacter: Character) {
        for (index, targetCharacter) in targetWord.enumerated() {
            if targetCharacter == inputCharacter {
                currentWord[index] = inputCharacter
            }
        }
        
        hasWordBeenCompleted()
    }
    
    private  func incorrectGuess() {
        incorrectGuessCount += 1
    }
    
    @objc private func updateCountdownTimer() {
        timeLeft -= 1
    }
    
    //MARK: - Public
    
    ///Starts the game
    ///
    ///Once called the **GameEngineDelegate** will start receiving updates regarding the status of the game.
    func startGame() {
        state = .playing
    }
    
    /// Method used to play a turn.
    ///
    /// Use this method by passing a character to the `GameEngine`. **GameEngineDelegate** will be called with the result of this turn.
    /// - Parameter character: input for the `GameEngine` to check if this character exists within the goal word.
    func play(_ character: Character) {
        if targetWord.contains(character) {
            correctGuess(character)
            
        } else {
            incorrectGuess()
        }
    }
}
