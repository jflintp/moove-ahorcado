//
//  MockViewController.swift
//  Moove Ahorcado
//
//  Created by SFY on 14/04/2020.
//  Copyright © 2020 SFY. All rights reserved.
//

import UIKit

class MockViewController: UIViewController {
    @IBOutlet weak var wordToGuessTextfield: UITextField!
    @IBOutlet weak var pointsIfCompletedTextfield: UITextField!
    @IBOutlet weak var pointsIfFailedTextfield: UITextField!
    @IBOutlet weak var timeLimitInSecondsTextfield: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        }
    
    @IBAction func startMockGamePressed(_ sender: Any) {
        let nextViewController = HangmanGameViewController(nibName: "GameViewController", bundle: nil)
        
        // Mock Data Model dictionary
        let dataModel = ["Palabra" : (wordToGuessTextfield.text?.isEmpty ?? true) ? "quedate" : wordToGuessTextfield.text!,
                         "timeLimit" : (timeLimitInSecondsTextfield.text?.isEmpty ?? true) ? "120" : timeLimitInSecondsTextfield.text!,
                         "puntosBien" : (pointsIfCompletedTextfield.text?.isEmpty ?? true) ? "100" : pointsIfCompletedTextfield.text!,
                         "puntosMal" : (pointsIfFailedTextfield.text?.isEmpty ?? true) ? "-50" : pointsIfFailedTextfield.text!]
        nextViewController.dataModel = dataModel
        nextViewController.modalPresentationStyle = .fullScreen
        present(nextViewController, animated: false, completion: nil)
    }
}
