//
//  LetterCollectionViewCell.swift
//  Moove Ahorcado
//
//  Created by SFY on 08/04/2020.
//  Copyright © 2020 SFY. All rights reserved.
//

import UIKit

class LetterCollectionViewCell: UICollectionViewCell {
    
    //MARK: IBOutlets
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var letterLabel: UILabel!
    
    //MARK: Properties
    /// A `Character` value which the cell can hold.
    public var value: Character = "_" {
        didSet {
            letterLabel.text = String(value.uppercased())
        }
    }
        
    private(set) var isEnabled = true {
        didSet {
            backgroundImage.image = UIImage(named: isEnabled ? "key_on" : "key_off")
        }
    }
    
    //MARK: Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //MARK: - Public
    /// Used to let the cell configure it's UI for when the user has interacted with it.
    public func pushed() {
        isEnabled = false
    }
}
