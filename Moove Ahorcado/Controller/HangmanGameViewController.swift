//
//  ViewController.swift
//  Moove Ahorcado
//
//  Created by SFY on 07/04/2020.
//  Copyright © 2020 SFY. All rights reserved.
//
//TODO: - Think about JSON entry data for words.
//TODO: - Add the close button to be configured if in case it's not needed.

import UIKit
import Lottie

/// **HangmanGameViewController's** delegate used to know when the game has finished and it's outcome.
///
/// Will receive either one of the following calls when Hangman Game has finnished:
/// * `gameFinishedSuccessfully:score`
/// * `gameFinishedUnsucessfully:score`
protocol HangmanGameDelegate: AnyObject {
    
    /// Called when game has finnished Successfully.
    /// - Parameter points: Points given for successful game.
    func gameFinishedSuccessfully(points: Int)
    
    /// Called when game has finnished Unsuccessfully.
    /// - Parameter points: Points given for unsuccessful game.
    func gameFinishedUnsucessfully(points: Int)
}

//MARK: -

final class HangmanGameViewController: UIViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var alphabetCollectionView: UICollectionView!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var targetWord: UILabel!
    @IBOutlet weak var movesLeftLabel: UILabel!
    @IBOutlet weak var mainBackgroundImage: UIImageView!
    @IBOutlet weak var animationContainerView: UIView!
    @IBOutlet weak var alphabetCollectionViewHeightConstraint: NSLayoutConstraint!
    
    //MARK: Delegate
    weak var delegate: HangmanGameDelegate?
    
    //MARK: Properties
    //TODO: Change dataModel to accept [String : Any]
    public var dataModel: [String : String]?
    public var gameModel = GameEngine()
    
    private let timeFormatter: DateComponentsFormatter = {
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .positional
        formatter.allowedUnits = [.minute, .second]
        formatter.zeroFormattingBehavior = [.pad]
        
        return formatter
    }()
    
    //MARK: IBActions
    @IBAction func closeButtonPressed(_ sender: Any) {
        dismiss(animated: false, completion: nil)
    }
    
    //MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
        gameModel.startGame()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupAnimation()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        alphabetCollectionViewHeightConstraint.constant = alphabetCollectionView.contentSize.height
    }
    
    fileprivate func setupGame() {
        if let dataModel = dataModel {
            gameModel = GameEngine(data: dataModel)
            gameModel.delegate = self
        }
    }
    
    fileprivate func setupAnimation() {
        let fileName = "bg_ahorcado"
        let animationView = AnimationView(name: fileName)
        animationView.translatesAutoresizingMaskIntoConstraints = false
        animationView.loopMode = .loop
        animationView.contentMode = .scaleAspectFit
        animationContainerView.addSubview(animationView)
        
        NSLayoutConstraint.activate([
            animationView.topAnchor.constraint(equalTo: animationContainerView.topAnchor),
            animationView.bottomAnchor.constraint(equalTo: animationContainerView.bottomAnchor),
            animationView.leadingAnchor.constraint(equalTo: animationContainerView.leadingAnchor),
            animationView.trailingAnchor.constraint(equalTo: animationContainerView.trailingAnchor)
        ])
    
        animationView.play()
    }
    
    fileprivate func setup() {
        setupGame()
        updateTargetWordUI(String(gameModel.currentWord))
        updatePointsUI(0)
        movesLeftLabel.text = NSLocalizedString("moves_left", comment: "Number of moves the user has left before it's game over.") + " " +  String(gameModel.maxMoves)
        imageView.image = nil
        
        alphabetCollectionView.dataSource = self
        alphabetCollectionView.delegate = self
        alphabetCollectionView.register(UINib(nibName: "LetterCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "LetterCollectionViewCell")
        
        let numberOfCellsPerRow: CGFloat = 7
        let sizeOfSideMargins: CGFloat = 42
        let collectionViewWidth = UIScreen.main.bounds.width - sizeOfSideMargins
        let flowLayout = UICollectionViewCenterLayout()
        flowLayout.itemSize = CGSize(width: collectionViewWidth / numberOfCellsPerRow , height: collectionViewWidth / numberOfCellsPerRow)
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.minimumLineSpacing = 0
        alphabetCollectionView.setCollectionViewLayout(flowLayout, animated: false)
    }
    
    //MARK: Private
    
    fileprivate func timeFormatted(_ totalSeconds: Int) -> String {
        let duration = TimeInterval(totalSeconds)
        guard let formattedDuration = timeFormatter.string(from: duration) else { return "" }
        return formattedDuration
    }
    
    fileprivate func updateTargetWordUI(_ word: String) {
        guard let font = UIFont(name: "Poppins-Bold", size: 18) else { fatalError("Poppins-Bold font does not exist") }
        
        let attributedString = NSMutableAttributedString(string: word.uppercased())
        attributedString.addAttribute(NSAttributedString.Key.font, value: font, range: NSRange(location: 0, length: attributedString.length))
        attributedString.addAttribute(NSAttributedString.Key.kern, value: 3, range: NSRange(location: 0, length: attributedString.length - 1))
        targetWord.attributedText = attributedString
    }
    
    fileprivate func updatePointsUI(_ points: Int) {
        pointsLabel.text = "\(points)" + NSLocalizedString("points_trailing_text", comment: "Flavor text showed after the amount of points scored")
    }
}

//MARK: - UICollectionViewDataSource
extension HangmanGameViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        gameModel.alphabet.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LetterCollectionViewCell", for: indexPath) as? LetterCollectionViewCell else  {
            return UICollectionViewCell()
        }
        let letterInAlphabet = gameModel.alphabet[indexPath.row]
        cell.value = letterInAlphabet
        return cell
        
    }
}

//MARK: - UICollectionViewDelegate
extension HangmanGameViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let cell = collectionView.cellForItem(at: indexPath) as? LetterCollectionViewCell {
            guard cell.isEnabled else { return }
            gameModel.play(cell.value)
            cell.pushed()
        }
    }
}

//MARK: - GameEngineDelegate
extension HangmanGameViewController: GameEngineDelegate {
    func countdownTimerChanged(timeLeft: Int) {
        timerLabel.text = timeFormatted(timeLeft)
    }
    
    func currentWordChanged(to word: String) {
        updateTargetWordUI(word)
    }
    
    func incorrectGuess(incorrectGuesses: Int, movesLeft: Int) {
        if let nextStepImage = UIImage(named: "step_\(incorrectGuesses)") {
            if let previousImage = imageView.image {
                imageView.image = previousImage.overlayed(with: nextStepImage)
            } else {
                imageView.image = UIImage(named: "step_\(incorrectGuesses)")
            }
        }
        movesLeftLabel.text = "Moves left: \(movesLeft)"
    }
    
    func gameCompleted(points: Int) {
        updatePointsUI(points)
        #if DEBUG
        let alert = UIAlertController(title: "Completed! Nice 👍", message: "You scored: \(gameModel.pointsForSuccessfulGame)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { _ in
            self.dismiss(animated: true, completion: nil)
        }))
        present(alert, animated: true, completion: nil)
        #endif
        delegate?.gameFinishedSuccessfully(points: points)
    }
    
    func countdownTimerEnded(timeLeft: Int, points: Int) {
        timerLabel.text = String(timeLeft)
        #if DEBUG
        let alert = UIAlertController(title: "Time ran out!", message: "Your total score: \(gameModel.pointsForSuccessfulGame)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { _ in
            self.dismiss(animated: true, completion: nil)
        }))
        self.present(alert, animated: true, completion: nil)
        #endif
        delegate?.gameFinishedUnsucessfully(points: points)
    }
    
    func gameOver(points: Int) {
        #if DEBUG
        let alert = UIAlertController(title: "Game Over!", message: "Looks like you ran out of turns! Better luck next time!", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { _ in
                        self.dismiss(animated: true, completion: nil)
        }))
        present(alert, animated: true, completion: nil)
        #endif
        delegate?.gameFinishedUnsucessfully(points: points)
    }
}
