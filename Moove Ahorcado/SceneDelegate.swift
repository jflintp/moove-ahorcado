//
//  SceneDelegate.swift
//  Moove Ahorcado
//
//  Created by SFY on 07/04/2020.
//  Copyright © 2020 SFY. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let _ = (scene as? UIWindowScene) else { return }
    }
}

