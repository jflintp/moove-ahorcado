//
//  Alphabets.swift
//  Moove Ahorcado
//
//  Created by SFY on 14/04/2020.
//  Copyright © 2020 SFY. All rights reserved.
//
//TODO: - Current Language assignment

import Foundation

/// Used as a namespace to store a dictionary of vaious alphabets for different languages.
struct Alphabets {
    
    
    /// A dictionary store for various language's alphabets.
    private static let allAlphabets: [String : [Character]] = ["es_ES" : ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","ñ","o","p","q","r","s","t","u","v","w","x","y","z"],
                                                               "en_ES" : ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","ñ","o","p","q","r","s","t","u","v","w","x","y","z"],
                                                               "es_CA" : ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"],
                                                               "en_US" : ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"]
    ]
    
    /// A method that attempts to retrieve the current user's system language alphabet.
    /// - Returns: An array of `Characters` (in order) for the current user's system language's alphabet. If non is found the default alphabet returned is **en_US**
    static func getUserLocalizedAlphabet() -> [Character] {
        guard let userLanguageAlphabet = allAlphabets[Locale.current.identifier] else {
            if let alphabet = allAlphabets["en_US"] {
                return alphabet
            }
            fatalError("en_US Alphabet does not exist in the allAlphabets array")
        }
        return userLanguageAlphabet
    }
}
 
